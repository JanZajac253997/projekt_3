#include <iostream>

using namespace std;

// tab[0] nie jest używane !!!
//-----------------------------------------------------------------------------------------------------------------------------------------------------
void tablica_plansza(char tab[])
{
  for(int i = 1; i <= 9; i++)
  {

    cout << " " << tab[i] << " ";
    if(i % 3)       cout << "|";               
    else if(i != 9) cout << "\n-----------\n"; 
    else            cout << endl;              
  }    
}
//     |   |  
//  -----------
//     |   |  
//  -----------
//     |   |  


// Sprawdzenie wygranej - 3 takie same symbole w linii
//-----------------------------------------------------------------------------------------------------------------------------------------------------
bool wygrana(char tab[], char symbol, bool flaga) //flaga jest po to, by byla roznica dla wywolania minimax - nie ma sygnalizowac to konca gry
{
  bool sprawdz;
  int i;
  
  sprawdz = false;                                                                          // true tylko wtedy, gdy pojawia sie 3 znaki w linii
  for(i = 1; i <= 7; i += 3) sprawdz |= ((tab[i] == symbol) && (tab[i+1] == symbol) && (tab[i+2] == symbol));// Sprawdzamy wiersze
  for(i = 1; i <= 3; i++)    sprawdz |= ((tab[i] == symbol) && (tab[i+3] == symbol) && (tab[i+6] == symbol));// Sprawdzamy kolumny  
  sprawdz |= ((tab[1] == symbol) && (tab[5] == symbol) && (tab[9] == symbol));                               // Sprawdzamy przekątną 1-5-9
  sprawdz |= ((tab[3] == symbol) && (tab[5] == symbol) && (tab[7] == symbol));                               // Sprawdzamy przekątną 3-5-7

  if(sprawdz)
  {
    if(!flaga)
    {
      tablica_plansza(tab);
      cout  << " WYGRYWA: "<< symbol <<"\n\n";
    }
    return true;
  }
  return false;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
bool remis(char tab[], bool flaga)
{
  for(int i = 1; i <= 9; i++)
  {
    if(tab[i] == ' ' || wygrana(tab,'O',false) || wygrana(tab,'X',false))
    {
      return false; // Jesli napotkamy spacje, to tablica_plansza posiada wolne pola - gra sie nie skonczyla
    }               // Jesli ktos wygral, to nie ma remisu
  }

  if(!flaga)                                    
  {                                             // Jesli nigdzie nie ma spacji to wszystkie pola sa zajete i nikt nie wygral - remis
    tablica_plansza(tab); cout << "\n REMIS \n\n";
  }
  return true;     
}


//-----------------------------------------------------------------------------------------------------------------------------------------------------
int minimax(char  tab[],int glebokosc, int alfa, int beta, char gracz){   
                                                                          
  int wyliczone, wartosc_ruchu;
  if (glebokosc == 0) return wartosc_ruchu;                        //jesli jestesmy na koncu rekurencji
  else if(wygrana(tab,gracz,true)) return (gracz == 'X') ? 1 : -1; // Czy bieżący gracz może wygrać? Jeśli tak, to zwracamy jego wygrywajaca wartosc: 1 lub -1
  else if(remis(tab,true)) return 0;                               // Czy dojdzie remisu? Jeśli tak, zwracamy wynik 0
  gracz = (gracz == 'X') ? 'O' : 'X';                              //Zmieniamy gracza na jego przeciwnika przy kazdym wywolaniu rekurencyjym

 if (gracz == 'X')              //symulacja ruchu X
 {                              //Uwaga! Wartosc -100 i 100 zostaly wybrane arbitralnie, moga byc, np. wieksze, ale tutaj mamy do dyspozycji wyniki tylko -1, 0, 1
 wartosc_ruchu = -100;          //Wartość wartosc_ruchu ustawiamy w zależności od tego, czyje ruchy analizujemy: 
                                //X maksymalizuje, wiec -100, a O minimalizuje, wiec 100
  for(int i = 1; i <= 9; i++)   //przeszukujemy "dzieci"
    if(tab[i] == ' ')
    {
       tab[i] = gracz;                                                    // Przeglądamy planszę szukając wolnych pół na ruch gracza. 
                                                                          //Na wolnym polu ustawiamy symbol X (lub O) i wyznaczamy wartość dla tego ruchu poprzez rekurencyjne wywolanie
       wyliczone = minimax(tab, glebokosc-1, alfa, beta, gracz);          // W tym miejscu de facto rekurencynie rozwiazywana jest cala gra
       tab[i] = ' ';                                                      // Planszę przywracamy do stanu przed podstawieniem
       wartosc_ruchu = (wartosc_ruchu > wyliczone) ? wartosc_ruchu : wyliczone;   //max
      alfa = (alfa > wyliczone) ? alfa : wyliczone;                               //max
      if (alfa >= beta) break;
    }
 }
  
else{                            //symulacja ruchu O
wartosc_ruchu = 100;  
  for(int i = 1; i <= 9; i++)   //przeszukujemy "dzieci"
    if(tab[i] == ' ')
      {
       tab[i] = gracz;                        // Przeglądamy planszę szukając wolnych pół na ruch gracza. Na wolnym polu ustawiamy literkę gracza i wyznaczamy wartość tego ruchu rekurencyjnym wywołaniem
       wyliczone = minimax(tab, glebokosc-1, alfa, beta, gracz);                // W tym miejscu de facto rekurencyjnie rozwiazywana jest cala gra i wynikiem jest m= -1, 0 lub 1
       tab[i] = ' ';                          // Planszę przywracamy
      wartosc_ruchu = (wartosc_ruchu < wyliczone) ? wartosc_ruchu : wyliczone;  //min
      beta = (beta < wyliczone) ? beta : wyliczone;                             //min
      }
    }
 return wartosc_ruchu;
}

// Ruch dla komputera - gracza X
//------------------------------------
int glebokosc = 7;            //UWAGA! zaczynamy od glebokosci 7, bo 1 ruch to czlowiek, a glebokosc 0 ma byc dla komputera na 2 polu, wiec zostaje 7 wolnych pol. 

int komputer(char tab[])
{
  int ruch, i, m, wartosc_ruchu;
  wartosc_ruchu = -100;        // to wyglada bardzo podobnie do minimax, ale minimax uznaje, ze ruch zostal zrobiony i analizuje odpowiedz O, potem znowu X, itd.
  for(i = 1; i <= 9; i++)     // wiec wykonujemy ten "pierwszy ruch" i sprawdzamy co przewidzi minimax dla takiego ruchu
    if(tab[i] == ' ')
    {
      tab[i] = 'X';                       //robimy "ruch" w pustym miejscu i wywolujemy minimax
      m = minimax(tab,glebokosc,-100,100,'X');  
      tab[i] = ' ';                       //"czyscimy" ruch
      if(m > wartosc_ruchu)
      {
        wartosc_ruchu = m; ruch = i;      //szukamy indeksu i, dla ktorego dostaniemy maksymalny wynik (1), ktory staje sie wartoscia naszego ruchu
      }        
    }
  return ruch;                            //ruszamy sie w miejsce i - dla tego indeksu wynik wyszedl najkorzystniejszy
  glebokosc -=2;    //nastepnym razem glebokosc zmniejszy sie o 2: ruch komputera i gracza zajma 2 dodatkowe miejsca
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------
bool zajete(char tab[], int r)    //zabezpieczenie przed oszukiwaniem/bledem
{
if (tab[r] == ' '){
  return false;
  }
if (r > 9) return true;
cout <<"zajete miejsce"<<endl;
return true;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------
void ruch(char tab[], char &gracz)
{
  int r;
  tablica_plansza(tab);
  if(gracz == 'O')                         //gracz wykonuje normalny ruch - nie 0 i nie na zajetym polu
  {
    cout << "\n ruch gracza (O): ";
    cin >> r;
    while (r==0 || zajete(tab,r)){         //nie mozna wybrac t[0] ani miejsca gdzie juz cos stoi
      cout <<"niedozwolony ruch!"<<endl;
      cout << "\n ruch O: ";               //wybieramy ponownie
    cin >> r;
    }
  }
  else
  {
    r = komputer(tab);    //ruch komputera
    cout << "\n ruch komputera (X): " << r << endl;
  }
  cout << "---------------------------\n\n";
  if((r >= 1) && (r <= 9) && (tab[r] == ' ')) tab[r] = gracz;   // sprawdzamy, czy nie popelniono bledu i nie zwrocono wartosci innej niz 1-9
                                                                //podstawiamy ruch w tablice i zmieniamy gracza
  gracz = (gracz == 'O') ? 'X' : 'O';   //zmiana gracza po wykonanym ruchu
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------





int main()
{
  char tab[10],gracz,wybor,pytanie;
  do                        //do while dlatego, ze najpierw gram, a potem dopiero pytam o ponowna gre. Zakladam, ze jak ktos wlacza program to chce zagrac
  {
    cout << "Kolko i Krzyzyk\n"<<endl;
    for(int i = 1; i <= 9; i++) {             // inicjalizacja pustych pol
      tab[i] = ' ';
      }
  cout << "Chcesz zaczac pierwszy? T/N" << endl;
  cin >> pytanie;
  while (pytanie != 'T' && pytanie != 'N' && pytanie != 't' && pytanie != 'n'){
    cout << "Sprobuj ponownie: "; 
  }
   if (pytanie == 'T' || pytanie == 't') gracz = 'O';                             // zaczyna O - czlowiek
   else gracz = 'X'; 
    while(!wygrana(tab,'X',false) && !wygrana(tab,'O',false) && !remis(tab,false)) {  // gra sie toczy do czasu az ktos wygra lub jest remis
      ruch(tab,gracz);                                                                // tutaj flaga jest inna niz dla wywolania minimax, bo faktycznie konczymy gre
      }

    cout << "Wcisnij Y, by zagrac jeszcze raz: ";                                     //mozliwosc zagrania wiecej niz raz
    cin >> wybor; 
    cout << "\n\n\n";

  } while((wybor == 'Y') || (wybor == 'y'));
  return 0;
}

/* PRZYKLAD GRY

Kolko i Krzyzyk

Chcesz zaczac pierwszy? T/N
t
   |   |          
-----------       
   |   |          
-----------       
   |   |          

 ruch gracza (O): 3
---------------------------

   |   | O 
-----------
   |   |   
-----------
   |   |   

 ruch komputera (X): 5     
---------------------------

   |   | O 
-----------
   | X |          
-----------       
   |   |          

 ruch gracza (O): 1
---------------------------

 O |   | O 
-----------
   | X |   
-----------
   |   |

 ruch komputera (X): 2
---------------------------

 O | X | O
-----------
   | X |
-----------
   |   |

 ruch gracza (O): 8
---------------------------

 O | X | O
-----------
   | X |
-----------
   | O |

 ruch komputera (X): 4
---------------------------

 O | X | O
-----------
 X | X |
-----------
   | O |

 ruch gracza (O): 6
---------------------------

 O | X | O
-----------
 X | X | O
-----------
   | O |

 ruch komputera (X): 9
---------------------------

 O | X | O
-----------
 X | X | O 
-----------
   | O | X

 ruch gracza (O): 7
---------------------------

 O | X | O
-----------
 X | X | O
-----------
 O | O | X

 REMIS

Wcisnij Y, by zagrac jeszcze raz:

*/